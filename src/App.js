import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { routes } from './routes/route';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        {routes.map(({url, components}, index) => <Route key={index} path={url} element={components}  />)}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
