import React from 'react'
import TrailerDesktop from './TrailerDesktop'
import { Desktop, Mobile, Tablet } from '../../layouts/Reponsive'
import TrailerTablet from './TrailerTablet'
import TrailerMobile from './TrailerMobile'

export default function ButtonTrailers({item}) {
  return (
    <>
        <Desktop>
            <TrailerDesktop item={item}/>
        </Desktop>
        <Tablet>
            <TrailerTablet item={item} /> 
        </Tablet>
        <Mobile>
            <TrailerMobile item={item} /> 
        </Mobile>
    </>
  )
}
