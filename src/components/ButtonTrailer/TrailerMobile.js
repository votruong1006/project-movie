import React from 'react'
import { Button, Modal } from "antd";
import { useState } from "react";

export default function TrailerMobile( {item} ) {
  const [modal1Open, setModal1Open] = useState(false);
  return (
    <>
    <Button
        className="w-14 h-14 -translate-y-3/4 rounded-full absolute top-1/2 left-8  bg-yellow-500 hover:bg-yellow-400 text-white invisible group-hover/slide:visible "
        type="primary"
        onClick={() => setModal1Open(true)}
    >
        <ion-icon style={{ fontSize: "30px" }} name="play"></ion-icon>
    </Button>
    <Modal
        cancelButtonProp={false}
        open={modal1Open}
        onOk={() => setModal1Open(false)}
        onCancel={() => setModal1Open(false)}
        footer={false}
        closable={false}
        className="md:w-2/3 w-full"
    >
        <figure className='container mx-auto flex-col justify-center'>
            <iframe className="w-full md:h-96 sm:h-72 h-52" src={item.trailer} title="YouTube video player"></iframe>
        </figure>
    </Modal>
</>
  )
}
