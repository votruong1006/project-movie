import React from 'react'
import UserLogin from './UserLogin'
import { Desktop, Mobile, Tablet } from '../../layouts/Reponsive'
import HeaderDeskTop from './HeaderDeskTop'
import HeaderTablet from './HeaderTablet'

export default function Header() {
    return (
      <>
            <Desktop>
                <HeaderDeskTop/>
            </Desktop>
            <Tablet>
                <HeaderTablet />
            </Tablet>
            <Mobile>
                <HeaderTablet />
            </Mobile>
      </>
    )
}

