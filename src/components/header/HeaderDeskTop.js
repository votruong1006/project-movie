import React, { useState } from 'react'
import UserLogin from './UserLogin'
import { NavLink } from 'react-router-dom'

export default function HeaderDeskTop() {
    const [fixed, setFixed] = useState(false)

    const handleScroll = () => {
        window.scrollY ? setFixed(true) : setFixed(false)
    }

    window.addEventListener("scroll", handleScroll)

    return (
       <>
            <div className={fixed ? 'block h-16 w-full' : 'hidden'}></div>
            <header className={fixed ? 'h-16 shadow-xl fixed top-0 left-0 z-50 w-full bg-white' : 'h-16 shadow-xl'}>
               <div className="container mx-auto flex h-full items-center">
                    <div className="logo w-1/3">
                        <NavLink to={'/'}>
                            <img className='h-14 cursor-pointer' src="http://demo1.cybersoft.edu.vn/logo.png" alt="logo" />
                        </NavLink>
                    </div>
                    <UserLogin />
               </div>
            </header>
       </>
    )
}

