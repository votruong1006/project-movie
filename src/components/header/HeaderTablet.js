import React, { useEffect, useRef, useState } from 'react'
import UserLogin from './UserLogin'
import { NavLink } from 'react-router-dom'

export default function HeaderTablet() {
    const [menu, setMenu] = useState(false)

    const menuRef = useRef()

    useEffect(() => {
        const handler = (e) => {
            if (!menuRef.current.contains(e.target)) {
                setMenu(false)
            }
        }

        document.addEventListener('mousedown', handler)
        
        // remove event when component un mounted
        return () => {
            document.removeEventListener('mousedown', handler)
        }
    }, [])

    // handle events
    const handleMenu = () => {
        setMenu(!menu)
    }


    return (
        <>
            <div className={menu ? 'fixed w-screen h-screen bg-black bg-opacity-50 z-50' : 'hidden fixed w-screen h-screen bg-black bg-opacity-50 z-50'}>
                <div ref={menuRef} className='absolute top-0 left-0 h-full w-44 bg-white'>
                    <div className='font-medium pt-5 '>
                        <UserLogin />
                    </div>
                </div>
            </div>
            <header className='h-16 shadow-xl'>
                <div className="md:container md:mx-auto mx-5 flex h-full items-center justify-between">
                    <div>
                        <NavLink to={'/'}>
                            <img className='md:h-14 h-12 cursor-pointer' src="http://demo1.cybersoft.edu.vn/logo.png" alt="logo" />
                        </NavLink>
                    </div>
                    <div>
                        <button onClick={handleMenu} className='bg-yellow-500 pt-1 px-3 hover:bg-yellow-400 duration-200'>
                            <ion-icon style={{ fontSize: '30px', color:'white' }} name="menu"></ion-icon>
                        </button>
                    </div>
                </div>
            </header>
        </>
    )
}
