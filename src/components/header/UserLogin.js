import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localService } from '../../services/localService'

export default function UserLogin() {
    const { account } = useSelector(state => state.userSlice)

// handle events 
    const handleLogout = () => {
        localService.remove()
        window.location.reload()
    }

    return (
        <>
            <div className='hidden lg:block w-2/3 font-medium'>
                <div className='flex justify-between items-center'>
                    <ul className='flex space-x-7 ml-14'>
                        <li className='cursor-pointer hover:text-yellow-500 duration-200'>Lịch Chiếu</li>
                        <li className='cursor-pointer hover:text-yellow-500 duration-200'>Cụm Rạp</li>
                        <li className='cursor-pointer hover:text-yellow-500 duration-200'>Tin Tức</li>
                    </ul>
                    <div className="flex login-page space-x-10 ">

                        {account ?
                            <>
                                <NavLink className='hover:text-yellow-500 duration-200 translate-y-2' to={'/login'}>
                                    <button>
                                        {account.hoTen}
                                    </button>
                                </NavLink>
                                <button onClick={handleLogout} className='px-4 py-2 bg-yellow-500 rounded text-white hover:bg-yellow-400 duration-200'>
                                    Đăng Xuất
                                </button>
                            </>
                            :
                            <>
                                <NavLink className='hover:text-yellow-500 duration-200 translate-y-2' to={'/login'}>
                                    <button>
                                        Log in
                                    </button>
                                </NavLink>
                                <NavLink to={'/register'}>
                                    <button className='px-4 py-2 bg-yellow-500 rounded text-white hover:bg-yellow-400 duration-200'>
                                        Get started
                                    </button>
                                </NavLink>
                            </>}

                    </div>
                </div>
            </div>

            <div className='lg:hidden flex flex-col items-center justify-center space-y-5'>
                <div className="flex flex-col space-y-5 ">

                    {account ?
                        <>
                            <NavLink className='hover:text-yellow-500 duration-200 ' to={'/login'}>
                                <button>
                                {account.hoTen}
                                </button>
                            </NavLink>
                            <button onClick={handleLogout} className='px-4 py-2 bg-yellow-500 rounded text-white hover:bg-yellow-400 duration-200'>
                                Đăng Xuất
                            </button>
                        </>
                        :
                        <>
                            <NavLink to={'/register'}>
                                <button className='px-4 py-2 bg-yellow-500 rounded text-white hover:bg-yellow-400 duration-200'>
                                    Get started
                                </button>
                            </NavLink>
                            <NavLink className='hover:text-yellow-500 duration-200 translate-x-7' to={'/login'}>
                                <button>
                                    Log in
                                </button>
                            </NavLink>
                        </>
                    }

                </div>
                <div className='h-1 w-full bg-yellow-500'></div>
                <ul className='space-y-5'>
                    <li className='cursor-pointer hover:text-yellow-500 duration-200'>Lịch Chiếu</li>
                    <li className='cursor-pointer hover:text-yellow-500 duration-200'>Cụm Rạp</li>
                    <li className='cursor-pointer hover:text-yellow-500 duration-200'>Tin Tức</li>
                </ul>
            </div>
        </>
    )
}
