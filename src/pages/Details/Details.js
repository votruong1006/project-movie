import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieServices } from "../../services/movieServ";
import moment from "moment/moment";
import { Progress } from "antd";
import { Tabs } from "antd";
const onChange = (key) => {
  console.log(key);
};
const items = [
  {
    key: "1",
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
];
export default function Details() {
  let param = useParams();
  let [movie, setMovie] = useState({});

  useEffect(() => {
    movieServices
      .getDetailMovies(param.id)
      .then((res) => {

        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderRap = () => {
    return movie.heThongRapChieu?.map((rap) => {
      return {
        key: rap.maHeThongRap,
        label: <img className="h-12 w-full border-b" src={rap.logo} alt="" />,
        children: <div className="p-3">{rap.cumRapChieu.map((cumRap) => {
          return (
            <div className="border-separate border-b">
              <p style={{ color: "rgb(139, 195, 74)", fontSize: "16px" }} className="font-medium p-2">{cumRap.tenCumRap}</p>
              <div className="grid gap-4 grid-cols-4  max-[800px]:grid-cols-3 max-[800px]:gap-3 max-[639.98px]:grid-cols-2 max-[639.98px]:gap-2">
                {cumRap.lichChieuPhim.map((lichChieu) => {
                  return (
                    <button style={{ backgroundColor: "rgba(246, 246, 246, 0.5)" }} className="rounded py-3 border text-red-500 font-medium shadow-gray-800 hover:font-black max-[800px]:hover:font-medium">
                      {moment(lichChieu.ngayChieuGioChieu).format(
                        "DD -mm-yyyy ~ hh:mm"
                      )}
                    </button>
                  );
                })}
              </div>
            </div>
          );
        })}</div>,
      };
    });
  };


  return (
    <div style={{ backgroundColor: "rgb(10, 32, 41)" }}>
      <div
        style={{ maxWidth: "960px" }}
        className=" space-y-5 container mx-auto px-6 w-full py-32 overflow-hidden"
      >
        <div className="  flex items-center justify-evenly  mb-20 max-[600px]:w-full max-[600px]:justify-center">
          <img className="w-1/4 h-4/5 rounded max-[600px]:w-1/2 max-[600px]:j" src={movie.hinhAnh} alt="" />
          <div className="px-5 space-y-5 text-white max-[600px]:w-1/2">
            <h2 className="text-2xl font-medium capitalize">{movie.tenPhim}</h2>
            <p className="text-xs ">{movie.moTa}</p>
            <p className="text-xl font-medium">
              Ngày khởi chiếu: {moment(movie.ngayKhoiChieu).format("DD-mm-yyyy")}
            </p>
            <button className="block px-5 py-2 rounded bg-yellow-400 font-medium text-white hover:bg-yellow-500">
              Mua vé
            </button>
          </div>
          <div className="p-10 text-center animate-pulse max-[600px]:hidden">
            <h2 className="font-medium text-xl mb-5 text-red-600 animate-pulse ">
              Đánh giá
            </h2>
            <Progress
              className="text-white"
              trailColor="white"
              strokeColor={{
                "0%": "#108ee9",
                "100%": "#87d068",
              }}
              format={(percent) => ` ${percent / 10} Điểm`}
              type="circle"
              percent={movie.danhGia * 10}
            />
          </div>
        </div>
        <div className="bg-white ">
          <Tabs
            tabPosition="left"
            defaultActiveKey="1"
            items={renderRap()}
            onChange={onChange}
          />
        </div>
      </div>
    </div>
  );
}
