import React from 'react'
import Slider from './Slider/Slider'
import ListMovie from './ListMovies/ListMovie'
import TabMovies from './TabsMovies/TabMovies'
import UngDung from './UngDung/UngDung'
import Footer from '../../components/Footer/Footer'


export default function Home() {
  return (
    <div className=''>
      <Slider />
      <ListMovie />
      <TabMovies />
      <UngDung />

    </div>
  )
}
