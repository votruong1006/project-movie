import React from 'react'
import ButtonTrailers from '../../../components/ButtonTrailer/ButtonTrailers'
import { NavLink } from 'react-router-dom'

export default function ItemMovie({ item }) {
    return (
        <>

            <div className='hidden sm:block md:mx-3 mx-1  space-y-2 shadow-2xl rounded h-full overflow-hidden relative group/slide'>
                <img className='w-full sm:h-[250px] md:h-[350px] rounded-t  ' src={item.hinhAnh} alt="img-movie" />
                <div className='md:pb-3 mx-2 group-hover/slide:hidden'>
                    <div className='space-x-5 flex'>
                        <p className='bg-yellow-500 p-1 rounded-full text-sm text-white'>C18</p> <p className='sm:text-sm md:text-base'>{item.tenPhim.substr(0, 20)}</p>
                    </div>
                    <p className='text-gray-500 text-sm mt-5'>{item.moTa.substr(0, 50) + '...'}</p>
                </div>
                <div className='hidden group-hover/slide:block  text-center w-full translate-y-1/2'>
                    <NavLink to={`/detail/${item.maPhim}`}>
                        <button className='bg-yellow-500 hover:bg-yellow-400 duration-200 w-[95%] mx-auto  py-3 text-white'>
                            Mua ve
                        </button>

                    </NavLink>
                </div>
                <ButtonTrailers item={item} />
            </div>


            <div className='sm:hidden shadow-xl rounded border-2 border-yellow-500 flex relative group/slide'>
                <img className='w-[150px] h-[180px] rounded' src={item.hinhAnh} alt="img-movie" />
                <div className='flex justify-center items-center'>
                    <div className='px-2 space-y-2'>
                        <div className='space-x-5 flex'>
                            <p className='bg-yellow-500 p-1 rounded-full text-sm text-white'>C18</p> <p className='text-base'>{item.tenPhim.substr(0, 20)}</p>
                        </div>
                        <p className='text-gray-500 text-sm mt-2'>{item.moTa.substr(0, 70) + '...'}</p>
                        <div className='text-center'>
                            <NavLink to={`/detail/${item.maPhim}`}>
                                <button className='px-5 text-xl text-white rounded bg-yellow-500 hover:bg-yellow-400 duration-200 w-full'>
                                    Mua ve
                                </button>
                            </NavLink>
                        </div>
                    </div>
                </div>
                <ButtonTrailers item={item} />
            </div>

        </>
    )
}
