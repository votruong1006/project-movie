import React, { useEffect, useState } from 'react'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { movieServices } from '../../../services/movieServ';
import ItemMovie from './ItemMovie';
import '../../../styles/listmovie.css'

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 4
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 3,
        slidesToSlide: 2 // optional, default to 1.
      },
};


export default function ListMovie() {
    const [listMovie, setListMovie] = useState([])

    useEffect(() => {
        const fetchListMovies = async () => {
            try {
                const res = await movieServices.getListMovies()
                setListMovie(res.data.content)
            } catch (error) {}
        }

        fetchListMovies()
    }, [])
    
    const renderListMovies = () => (
        listMovie.map((item, index) => (
            <ItemMovie item={item} key={index} />
        ))
    )

    return (
        <section className='pb-20 container mx-auto'>
            <div className='hidden sm:block'>
                <Carousel className='h-[550px]' autoPlay={true} autoPlaySpeed={3000} responsive={responsive} showDots={true} arrows={false}>
                    {renderListMovies()}
                </Carousel>
            </div>

            <div className='sm:hidden space-y-5 mx-5'>
                {renderListMovies()}
            </div>

        </section>
    )
}
