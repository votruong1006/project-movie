import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";

const contentStyle = {
    margin: 0,
    height: "550px",
    color: "#fff",
    width: "100%",
};

export default function ItemSlider({ item }) {
    const [modal1Open, setModal1Open] = useState(false);

    const { img, ytb } = item;
    return (
        <>
            <div className="w-full lg:h-full md:h-96 h-56 relative group/slide" style={contentStyle}>
                <img className="w-full h-full" src={img} alt="img-slide" />

                <>
                    <Button
                        className="w-20 h-20 -translate-x-1/2 -translate-y-1/2 rounded-full absolute top-1/2 left-1/2  bg-yellow-500 hover:bg-yellow-400 text-white invisible group-hover/slide:visible "
                        type="primary"
                        onClick={() => setModal1Open(true)}
                    >
                        <ion-icon style={{ fontSize: "60px" }} name="play"></ion-icon>
                    </Button>
                    <Modal
                        cancelButtonProp={false}
                        open={modal1Open}
                        onOk={() => setModal1Open(false)}
                        onCancel={() => setModal1Open(false)}
                        footer={false}
                        closable={false}
                        className="md:w-2/3 w-full"
                    >
                        <figure className='container mx-auto flex-col justify-center'>
                            <iframe className="w-full md:h-96 sm:h-72 h-52" src={ytb} title="YouTube video player"></iframe>
                        </figure>
                    </Modal>
                </>
            </div>
        </>
    );
}



