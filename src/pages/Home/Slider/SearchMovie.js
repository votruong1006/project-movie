import React, { useEffect, useState } from 'react'
import { movieServices } from '../../../services/movieServ'

export default function SearchMovie() {
    const [movieList, setMovieList] = useState([])

    useEffect(() => {
        const fetchListMovie = async () => {
            try {
                const res = await movieServices.getListMovies()
                setMovieList(res.data.content)
            } catch (error) { }
        }

        fetchListMovie()

    }, [])



    return (
        <>
            <div className="hidden absolute -bottom-8 h-20 w-3/4 text-center left-1/2 -translate-x-1/2 md:flex bg-white p-5 space-x-3 rounded shadow-xl">
                <select className='grow border-none outline-none hover:cursor-pointer' id="movie">
                    <option value="0">Phim</option>
                </select>
                <select className='grow border-none outline-none hover:cursor-pointer' id="theater">
                    <option value="0">Rap</option>
                </select>
                <select className='grow border-none outline-none hover:cursor-pointer' id="date">
                    <option value="0">Ngay Gio Chieu</option>
                </select>
                <button className='bg-yellow-500 px-5 text-white rounded py-1 hover:bg-yellow-400 duration-200'>
                    Mua Ve Ngay
                </button>
            </div>

            <form className="md:hidden w-[90%] rounded-xl flex items-center justify-center container mx-auto absolute -bottom-20 left-1/2 -translate-x-1/2 shadow-2xl border border-yellow-500">
                <div className='w-[93%]'>
                    <input className='w-full rounded-xl border-none outline-none focus: shadow-none focus:outline-none focus:border-none' placeholder="Tìm kiếm phim" type="text" />
                </div>
                <button className='w-[7%] rounded-full text-yellow-500 hover:text-yellow-400 duration-200' type="button ">
                    <ion-icon style={{fontSize: '20px'}} name="search"></ion-icon>
                </button>
            </form>


        </>
    )
}
