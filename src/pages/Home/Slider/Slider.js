import React, { useEffect, useState } from 'react'
import { img_carousel } from '../../../data/home';
import { Carousel } from 'antd';
import ItemSlider from './ItemSlider';
import SearchMovie from './SearchMovie';


const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <button
      className={`${className} flex text-gray-500 hover:text-gray-400 duration-200`}
      style={{ ...style, height: '65px', width: '65px', display: "block", 'borderRadius': '50%', 'marginRight': '55px', color: 'white' }}
      onClick={onClick}
    >
      <ion-icon style={{ fontSize: '50px' }} name="chevron-forward-outline"></ion-icon>
    </button>
  );
}

const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <button
      className={`${className} flex text-gray-500 hover:text-gray-400 duration-200`}
      style={{ ...style, height: '65px', width: '65px', display: "block", 'borderRadius': '50%', 'marginLeft': '45px', 'zIndex': '10', color: 'white' }}
      onClick={onClick}
    >
      <ion-icon style={{ fontSize: '50px' }} name="chevron-back-outline"></ion-icon>
    </button>
  );
}


// ----------------------------------------------------------------
export default function Slider() {


  const settings = {
    dots: window.innerWidth >= 688 ? true : false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    appendDots: dots => (
      <div
        style={{
          paddingBottom: '75px'
        }}
      >
        <ul> {dots} </ul>
      </div>
    ),
    customPaging: i => (
      <div
        className='bg-yellow-500 hover:bg-yellow-400 duration-200'
        style={{
          width: "15px",
          height: '15px',
          borderRadius: '50%',
        }}
      >

      </div>
    ),
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />
  };


  // render Slider
  const renderSlider = () => {
    return img_carousel.map((item, index) => <ItemSlider item={item} key={index} />)
  }


  // ----------------------------------------------------------------
  return (
    <section className='relative' >

      <Carousel  {...settings} arrows autoplay autoplaySpeed={4000} >
        {renderSlider()}
      </Carousel>

      <SearchMovie />
     
    </section>
  )
}
