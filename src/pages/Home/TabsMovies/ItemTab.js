import moment from 'moment';
import React from 'react'
import { NavLink } from 'react-router-dom';

export default function ItemTab({ phim }) {
    return (
        <div className="flex items-center space-x-5 border-separate border-b pb-6 px-2">
            <img
                className="h-32 w-24 object-cover object-top rounded"
                src={phim.hinhAnh}
                alt=""
            />
            <div className='grow'>
                <h5 className="font-medium text-xl mb-5 ">{phim.tenPhim}</h5>
                <div className="grid gap-3 grid-cols-2 ">
                    {phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
                        return (
                            <NavLink style={{ backgroundColor: "rgba(246, 246, 246, 0.5)" }} to={`/detail/${phim.maPhim}`}><button className="rounded p-2 border text-red-500 font-medium w-full hover:font-bold ">
                                {moment(item.ngayChieuGioChieu).format("DD -mm-yyyy ~ hh:mm")}
                            </button></NavLink>
                        );
                    })}
                </div>
            </div>
        </div>
    )
}
