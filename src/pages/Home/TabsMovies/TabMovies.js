import React, { useEffect, useState } from "react";
import { movieServices } from "../../../services/movieServ";
import { Tabs } from "antd";
import ItemTab from "./ItemTab";
const onChange = (key) => {
    console.log(key);
};
const items = [
    {
        key: "1",
        label: `Tab 1`,
        children: `Content of Tab Pane 1`,
    },
];
export default function TabMovies() {
    const [heThongRap, setHeThongRap] = useState([]);
    useEffect(() => {
        movieServices
            .getMovieByTheater()
            .then((res) => {
                console.log(res);
                setHeThongRap(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    let renderHeThongRap = () => {
        return heThongRap.map((rap) => {
            return {
                key: rap.maHeThongRap,
                label: <img className="h-14 w-14 " src={rap.logo} alt="" />,
                children: (
                    <Tabs
                        style={{ height: 700 }}
                        tabPosition="left"
                        defaultActiveKey="1"
                        items={rap.lstCumRap.map((cumRap) => {
                            return {
                                key: cumRap.tenCumRap,
                                label: <div className="w-60 h truncate  ">
                                    <p className="font-medium">{cumRap.tenCumRap}</p>
                                    <p className="text-xs text-gray-600">{cumRap.diaChi}</p>
                                </div>,
                                children: <div
                                    style={{ height: 700, overflowY: "scroll" }}
                                    className="space-y-5"
                                >
                                    {cumRap.danhSachPhim.map((phim) => {
                                        return <ItemTab phim={phim} />;
                                    })}
                                </div>,
                            };
                        })}
                        onChange={onChange}
                    />
                ),
            };
        });
    };
    return (
        <div style={{ maxWidth: "960px" }} className="max-[960px]:hidden  px-6 w-full container py-20 mx-auto  overflow-hidden">

            <Tabs
                className="border"
                style={{ height: 700 }}
                tabPosition="left"
                defaultActiveKey="1"
                items={renderHeThongRap()}
                onChange={onChange}
            />
        </div>
    );
}
