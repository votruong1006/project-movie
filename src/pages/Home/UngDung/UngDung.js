import React from 'react'

export default function UngDung() {
    return (
        <div className='w-full h-full py-16' style={{ backgroundImage: `url("https://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg")`, backgroundSize: "contain" }}>
            <div style={{ maxWidth: "960px" }} className="container mx-auto flex flex-wrap px-6  max-[960px]:justify-center max-[960px]:items-center max-[960px]:flex-row">

                <div className="space-y-5 w-1/2 pt-14  max-[960px]:text-center max-[960px]:w-full">
                    <p className='text-white font-bold text-3xl max-[600px]:text-xl'>Ứng dụng tiện lợi dành cho</p>
                    <p className='text-white font-bold text-3xl max-[600px]:text-xl'>người yêu điện ảnh</p>
                    <p className='text-white font-medium text-base  pb-8'>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</p><br />
                    <button className=' rounded px-8 py-5 bg-red-600 text-white font-medium text-base uppercase hover:bg-red-800 max-[960px]:py-3' href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197">
                        <a>App miễn phí - Tải về ngay!</a>

                    </button>
                    <p className='text-white font-medium text-base pt-5 max-[600px]:text-sm'>TIX có hai phiên bản
                        <a className='cursor-pointer border-b border-white' href="https://apps.apple.com/us/app/123phim-mua-ve-lien-tay-chon/id615186197">IOS</a>&<a className='cursor-pointer border-b border-white' href="https://play.google.com/store/apps/details?id=vn.com.vng.phim123">Android</a>
                    </p>
                </div>
                <div className="w-1/2 max-[960px]:w-full">

                </div>

            </div>
        </div>
    )
}
