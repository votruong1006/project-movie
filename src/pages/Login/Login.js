import React from "react";
import { Button, Form, Input, message } from "antd";
import { userService } from "../../services/userService";
import { useDispatch } from "react-redux";
import { setUserLogin } from "../../toolkits/userSlice";
import { useNavigate } from "react-router-dom";
import { localService } from "../../services/localService";

export default function Login() {
// use hook 
  const dispatch = useDispatch()
  const navigate = useNavigate()

// handle events
  const onFinish = (account) => {
    const postAccount = async () => {
      try {
        const res = await userService.login(account)
        dispatch(setUserLogin(res.data.content))
        localService.set(res.data.content)
        message.success('Đăng nhập thành công !')
        navigate('/')
      } catch (error) {
        message.error('Đăng nhập thất bại ')
      }
    }
    postAccount()
  };
 
// ----------------------------------------------------------------
  return (
    <section
      className="min-h-screen flex justify-center items-center"
      style={{
        backgroundImage:
          "url(http://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)",
      }}
    >
     <div className=" bg-white rounded  w-[400px] p-10 space-y-5">
     <h1 className="text-center"> <ion-icon style={{color: '#C27801', fontSize: '65px'}} name="person-circle"></ion-icon></h1>
     <h2 className="text-center text-2xl font-semibold tracking-wider">Đăng Nhập </h2>
     <Form
    name="basic"
    labelCol={{
      span: 8,
    }}
    wrapperCol={{
      span: 24,
    }}
    className="w-full"
    initialValues={{
      remember: true,
    }}
    onFinish={onFinish}
    autoComplete="off"
    layout="vertical"
  >
   
   <Form.Item
      label="Username"
      name="taiKhoan"
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
    >
      <Input />
    </Form.Item>

    <Form.Item
      label="Password"
      name="matKhau"
      rules={[
        {
          required: true,
          message: 'Please input your password!',
        },
      ]}
    >
      <Input.Password />
    </Form.Item>

    <Form.Item
      wrapperCol={{
        offset: 0,
        span: 24,
      }}
    >
      <Button type="primary" className="bg-yellow-500 hover:bg-yellow-400 w-full" htmlType="submit">
        Submit
      </Button>
    </Form.Item>
  </Form>
     </div>
    </section>
  );
}
