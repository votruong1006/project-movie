import React from 'react'
import {
    Button,
    Form,
    Input,
    message,
} from 'antd';

import { userService } from '../../services/userService';
import { useDispatch } from 'react-redux';
import { setUserLogin } from '../../toolkits/userSlice';
import { localService } from '../../services/localService';
import { useNavigate } from 'react-router-dom';



export default function Register() {
    const [form] = Form.useForm();
    const dispatch = useDispatch()
    const navigate = useNavigate();

    const onFinish = (values) => {
        const registerAccount = async () => {
            try {
                const res = await userService.register(values)
                message.success('Đáng kí tài khoản thành công !')
                dispatch(setUserLogin(res.data.content))
                localService.set(res.data.content)
                navigate('/')
            } catch (error) {
                message.error(error.response.data.content)
            }
        }
        registerAccount()
    };

    return (
        <section
            className="min-h-screen flex justify-center items-center"
            style={{
                backgroundImage:
                    "url(http://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)",
            }}
        >
            <div className=" bg-white rounded  w-[400px] p-5 space-y-3">
                <h1 className="text-center"> <ion-icon style={{ color: '#C27801', fontSize: '65px' }} name="key"></ion-icon></h1>
                <h2 className="text-center text-2xl font-semibold tracking-wider">Đăng Ký </h2>
                <Form
                    form={form}
                    name="register"
                    onFinish={onFinish}
                    initialValues={{
                        residence: ['zhejiang', 'hangzhou', 'xihu'],
                        prefix: '86',
                    }}
                    className='w-full'
                    scrollToFirstError
                    layout='vertical'
                    wrapperCol={24}
                    labelCol={24}
                    autoComplete='off'
                >
                    <Form.Item
                        name="hoTen"
                        label="Họ và Tên"
                        tooltip="What do you want others to call you?"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tài khoản !',
                                whitespace: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="taiKhoan"
                        label="Tài Khoản"
                        tooltip="What do you want others to call you?"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập họ và tên !',
                                whitespace: true,
                            },
                            {
                                min: 8,
                                message: 'Tài khoản tối thiểu 8 kí tự !',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label="E-mail"
                        rules={[
                            {
                                type: 'email',
                                message: 'Chưa đúng định dạng email !',
                            },
                            {
                                required: true,
                                message: 'Vui lòng nhập email của bạn !',
                            },
                        ]}
                        hasFeedback
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        name="matKhau"
                        label="Mật Khẩu"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập mập khẩu!',
                            },
                            {
                                min: 8,
                                message: 'Tối thiểu 8 kí tự'
                            }
                        ]}
                        hasFeedback
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        name="confirm"
                        label="Xác nhận mật khẩu"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng xác nhận mật khẩu !',
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('matKhau') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error('Mật khẩu chưa trùng khợp !'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" className='w-full bg-yellow-500 hover:bg-yellow-400' htmlType="submit">
                            Register
                        </Button>

                    </Form.Item>
                </Form>
            </div>
        </section>
    )
}
