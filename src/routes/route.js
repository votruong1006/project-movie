import Layout from "../layouts/Layout";
import Details from "../pages/Details/Details";
import Home from "../pages/Home/Home";
import Login from "../pages/Login/Login";
import Register from "../pages/Register/Register";

export const routes = [
    {
        url: '/',
        components: <Layout Components={Home} />
    },
    {
        url: '/home',
        components: <Layout Components={Home} />
    },
    {
        url: '/detail/:id',
        components: <Layout Components={Details} />
    },
    {
        url: '/login',
        components: <Login />
    },
    {
        url: '/register',
        components: <Register />
    }
]