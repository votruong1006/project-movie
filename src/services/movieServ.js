import { https } from "./config";

export const movieServices = {
    getListMovies: () => https.get('/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04'),
    getListTheaters: () => https.get(),
    getDetailMovies: (id) => https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`),
    getMovieByTheater: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
    },
}