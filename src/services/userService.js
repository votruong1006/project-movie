import { https } from "./config";

export const userService = {
    login: (account) => https.post('/api/QuanLyNguoiDung/DangNhap', account),
    register: (account) => https.post('/api/QuanLyNguoiDung/DangKy', account)
}