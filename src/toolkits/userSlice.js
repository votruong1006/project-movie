import { createSlice } from '@reduxjs/toolkit'
import { localService } from '../services/localService';

const initialState = {
  // if local don't have account return null 
  account: localService.get()
}

const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {
    setUserLogin: (state, action) => {
      state.account = action.payload
    }
  }
});

export const { setUserLogin } = userSlice.actions

export default userSlice.reducer